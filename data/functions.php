<?php
session_start();


function LoggedIn(){

  include("db_config.php");
  $logged = 0;  
  if(!empty($_SESSION["key"])){
    
    $res=mysql_connect($HOST,$USER,$PWD);
    mysql_select_db($DATABASE); 
    $sql="SELECT * FROM `user` WHERE `key` = '" . $_SESSION["key"] . "' LIMIT 1";
    $result = mysql_query($sql); 
    
    if(mysql_num_rows($result)>0) { 
      $logged = 1;  
    }
  }
  
  return $logged;
}
function PageValid(){
  return true;
}
function set_login_key($key){
  $_SESSION['key'] = $key;
  return true;
}
function showDays($days){  
  $day = "";
  $string = "";
  $more = 0;
  for ($i = 1; $i <= 7; $i++) {
      $day = substr($days, $i,1); 
      if($day == "1"){
        if($more == 1){
          $string .=",";
        }    
        switch($i){
          case 1: $string .= "h"; break;
          case 2: $string .= "k"; break;
          case 3: $string .= "sze"; break;
          case 4: $string .= "cs"; break;
          case 5: $string .= "p"; break;
          case 6: $string .= "szo"; break;
          case 7: $string .= "v"; break;
        }
        $more = 1;
      }     
  }             
  return $string;  
} 
function encrypt($key,$string){
  $iv = mcrypt_create_iv(
    mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
    MCRYPT_DEV_URANDOM
  );
  
  $encrypted = base64_encode(
      $iv .
      mcrypt_encrypt(
          MCRYPT_RIJNDAEL_128,
          hash('sha256', $key, true),
          $string,
          MCRYPT_MODE_CBC,
          $iv
      )
  );
  return $encrypted;
}
function decrypt($encrypted,$key){
  $data = base64_decode($encrypted);
  $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));
  
  $decrypted = rtrim(
      mcrypt_decrypt(
          MCRYPT_RIJNDAEL_128,
          hash('sha256', $key, true),
          substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
          MCRYPT_MODE_CBC,
          $iv
      ),
      "\0"
  );
  return $decrypted; 
}
function generateKey($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>