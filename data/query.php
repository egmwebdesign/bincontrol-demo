<?php
require_once("functions.php");  
require("../codebase/connector/grid_connector.php");

include("db_config.php");
$res = mysql_connect($HOST,$USER,$PWD);
mysql_select_db($DATABASE);  

if($_GET['cmd']=="login"){
  $status = "bad";
  $user = $_POST['login'];
  $pwd = $_POST['pwd'];
  
  $sql="SELECT * FROM `user` WHERE username = '$user' LIMIT 1";
  $result = mysql_query($sql); 
  $data = mysql_fetch_assoc($result);
  
  
  if(decrypt($data['hash'],$pwd) == $data['key']){
    set_login_key($data['key']);
    $status = "good";    
  }
  
  echo $status;
}

LoggedIn() ? true : exit; 

if($_GET['cmd']=="contract"){
  $conn = new GridConnector($res);           
  $conn->set_encoding("iso-8859-2");
  $conn->render_table("contract","contract_id","code,address,days,chip"); 
}

if($_GET['cmd']=="undetected_contract"){
  $conn = new GridConnector($res);             
  $conn->set_encoding("iso-8859-2");
  
  $sql="SELECT *,c.chip FROM contract c LEFT JOIN detect d ON c.chip=d.chip WHERE d.detect_id IS NULL ";
   
  $result = mysql_query($sql);    
  $x = 0;
  while ($row = mysql_fetch_assoc($result)) {  
    $data[$x] = $row;  
    $data[$x]['days'] = showDays($row['days']);
    $x++;
  }                
  
  $conn->render_array($data, "contract_id", "code,address,days,chip");
}

if($_GET['cmd']=="detect"){
  $conn = new GridConnector($res);             
  $conn->set_encoding("iso-8859-2");
  
  $sql="SELECT * FROM contract c RIGHT OUTER JOIN detect d ON c.chip=d.chip";
   
  $result = mysql_query($sql);    
  $x = 0;
  while ($row = mysql_fetch_assoc($result)) {  
    $data[$x] = $row;  
    $data[$x]['days'] = showDays($row['days']);
    $x++;
  }                
  
  $conn->render_array($data, "detect_id", "plate_number,detect_time,chip,_code,position,space,days,code,address");
}
if($_GET['cmd']=="export_csv"){
  header("Content-type: text/csv");
  header("Content-Disposition: attachment; filename=export.csv");
  header("Pragma: no-cache");
  header("Expires: 0");
  
  echo $_POST['text'];
}
?>