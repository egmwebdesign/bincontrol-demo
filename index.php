<?php
require_once("data/functions.php");

if(LoggedIn()){   ?>
  <!DOCTYPE html>
  <html>
    <head>
      <title>BinControl próbafeladat</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <script src="codebase/dhtmlx_pro.js" type="text/javascript"></script>
      <script src="codebase/dhtmlx_custom.js" type="text/javascript"></script>
      <link rel="stylesheet" type="text/css" href="skins/material/dhtmlx.css">
      <link rel="stylesheet" type="text/css" href="codebase/dhtmlxcalendar.css">  
      <link rel="stylesheet" type="text/css" href="codebase/style.css">    
    </head> 
    <body>
    <div id="menuObj"></div>
    <?php 
    $page = "area1"; 
    if(!empty($_GET['page']) && file_exists("pages/" . $_GET['page'].".php")){
      $page = $_GET['page'];   
    }
    require_once("pages/" . $page.".php");
    ?>  
    </body>
  </html>   
<?php 

}else{
  require_once("pages/login.php");
}
?>
