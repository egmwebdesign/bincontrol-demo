dhtmlXGridObject.prototype._in_header_daterange_filter=function(a,b){
                            
            a.innerHTML="<div style='width:100%; margin:0 auto; text-align: left'><input type='text' id='datefrom' class='datefrom'>-tól<br><input type='text' id='dateto' class='dateto'>-ig</div>";
         
            a.onclick=a.onmousedown=function(a){
               return(a||event).cancelBubble=!0
            };
            
            a.onselectstart=function(){
               return event.cancelBubble=!0
            };
                  
            datefrom = getChildElement(a.firstChild,"datefrom");
            dateto = getChildElement(a.firstChild,"dateto");
            
            myCalendar = new dhtmlXCalendarObject([datefrom , dateto])
            myCalendar.setDateFormat("%Y-%m-%d %H:%i:%s");      //Date format MM/DD/YYY
            
            myCalendar.attachEvent("onClick",function(date){
                 mygrid.filterByAll();
            })
  
            this.makeFilter(datefrom ,b);
            this.makeFilter(dateto ,b);
                  
            datefrom._filter=function(){
               var a=this.value;
               return a==""?"":function(b){
                  
                  aDate = parseDate(a)
                  bDate = parseDate(b)   
                  return aDate <= bDate ;
                  
               }
            }
            
            dateto._filter=function(){
  
               var a=this.value;
               return a==""?"":function(b){
                  aDate = parseDate(a)
                  bDate = parseDate(b)   
                  return aDate >= bDate
               }      
            }
  
            this._filters_ready()      
            
         };
       
       // parse a date in mm/dd/yyyy format
       function parseDate(input) {   
         var parts = input.split(' ');
         var dateparts = parts[0].split('-');
         var timeparts = parts[1].split(':');
         // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])   
         return new Date(dateparts[0], dateparts[1], dateparts[2], timeparts[0], timeparts[1], timeparts[2]).getTime(); // Date format MM/DD/YYY
       }

       function getChildElement(element,id) {

          for (i=0;i<element.childNodes.length;i++)
          {
             if (element.childNodes[i].id == id)
                return element.childNodes[i];
          }
       
          return null
       }
       
function setMenuActive(id){
  document.querySelector("div[id*="+id+"] > div").style.textDecoration = 'underline';
  document.querySelector("div[id*="+id+"] > div").style.fontWeight = 'bold'; 
}