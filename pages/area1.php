<?php function_exists("PageValid") ? true : exit; ?>
<form id="ExportCSVForm" method="post" action="data/query.php?cmd=export_csv" target="CSV">
    <input type="hidden" id="export_csv_input" name="text" value="" />
</form>  

<script type="text/javascript">
      dhtmlxEvent(window,"load",function(){   
      
        var layout = new dhtmlXLayoutObject(document.body,"1C");         
        layout.cells("a").setText("Szerződések");
        
        var menu = layout.attachMenu();
        menu.setIconsPath("img/");
        menu.loadStruct("data/menu.xml",function(){
          setMenuActive("ftArea1");
        });
        

        
        var toolbar = layout.attachToolbar();
        toolbar.setIconsPath("img/");
        toolbar.loadStruct("data/toolbar_area1.xml");
        
        var pagingId = "pagingArea_"+window.dhx.newId();
        var statusbar = layout.attachStatusBar({
            text: "<div id='"+pagingId+"'></div>",
            paging: true
        });
        
        
        var contractsGrid = layout.cells("a").attachGrid();
        
        contractsGrid.setHeader("Sz.kód,Cím,Gy.napok,Chip");  
        contractsGrid.setColumnIds("code,address,days,chip");         
        contractsGrid.setInitWidths("250,250,*,250");  
        contractsGrid.setColAlign("left,left,left,left");    
        contractsGrid.setColTypes("ro,ro,ro,ro");              
        contractsGrid.setColSorting("str,str,str,str");  
        
        contractsGrid.i18n.paging={
              results:"Találatok",
              records:"Listázva ",
              to:" -től ",
              page:"Oldal ",
              perpage:"sor egy oldalon",
              first:"Első oldalhoz",
              previous:"Előző",
              found:"Found records",
              next:"Következő",
              last:"Utolsó oldalhoz",
              of:" -ból ",
              notfound:"Nincs találat" 
        };
        contractsGrid.setImagePath("codebase/imgs/");
        contractsGrid.enablePaging(true, 15, 5, pagingId);
        contractsGrid.setPagingSkin("toolbar");

        contractsGrid.init();
        contractsGrid.load("data/query.php?cmd=contract");  
        contractsGrid.attachHeader("#text_filter,#text_filter,,#text_filter");     



        toolbar.attachEvent("onclick",function(id){
          if(id=="export_csv"){       
            contractsGrid.setCSVDelimiter("|");                  
            var myCSVString = contractsGrid.serializeToCSV();
            document.getElementById('export_csv_input').value = myCSVString;
            window.open('', 'CSV');
            document.getElementById('ExportCSVForm').submit();
          };
        });  

      });
    </script>                 