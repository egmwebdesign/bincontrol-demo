<?php function_exists("PageValid") ? true : exit; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Log in</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="stylesheet" type="text/css" href="codebase/skins/dhtmlxform_dhx_skyblue.css">
	<script src="codebase/dhtmlxcommon.js"></script>
	<script src="codebase/dhtmlxform.js"></script>
	<link rel="stylesheet" type="text/css" href="codebase/style.css">
  
</head>
<body onload="doOnLoad();">
  <iframe border="0" frameBorder="0" name="submit_ifr" class="submit_iframe"></iframe>
  
	<form id="loginObj" target="submit_ifr">
    <div class="inputs">
      <label>Felhasználónév: </label>
      <input class="dhxlist_txt_textarea" validate="MyValidLogin" type="text" name="login" value="" />
    </div>
    <div class="inputs">
      <label>Jelszó: </label>
      <input class="dhxlist_txt_textarea" validate="MyValidPassword" type="password" name="pwd" value="" />
    </div>
    <div class="submit">
      <input type="submit" class="btn" value="Belépés" onclick="doLogin();">
    </div>                
        
    <span id="res">&nbsp;</span>
    <span>(admin/1)</span>
    
	</form>
	<script>
	var myForm, resObj;
	function doOnLoad(){
		dhtmlxValidation.isMyValidLogin = function(val){
			return true;
		}
		dhtmlxValidation.isMyValidPassword = function(val){
			return true;
		}
		myForm = new dhtmlXForm("loginObj");
		resObj = document.getElementById("res");
	}

	function doLogin() {
		if (!myForm.validate()) return; 
     
		myForm.send("data/query.php?cmd=login&etc="+new Date().getTime(), "post", function(response){
			if (response.xmlDoc.responseText == "good") {
				 resObj.innerHTML = "Sikeres belépés!";
				 resObj.style.color = "green";
         location.reload();   
			} else {
				 resObj.innerHTML = "Sikertelen belépés!";
				 resObj.style.color = "red";
			}  
		});
	}

</script>
</body>
</html>