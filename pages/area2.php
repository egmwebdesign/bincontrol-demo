<?php function_exists("PageValid") ? true : exit; ?>
<form id="ExportCSVForm" method="post" action="data/query.php?cmd=export_csv" target="CSV">
    <input type="hidden" id="export_csv_input" name="text" value="" />
</form>
<script type="text/javascript">     
      dhtmlxEvent(window,"load",function(){   
      
        var layout = new dhtmlXLayoutObject(document.body,"1C");         
        layout.cells("a").setText("Detekciók");
        
        var menu = layout.attachMenu();
        menu.setIconsPath("img/");
        menu.loadStruct("data/menu.xml",function(){
          setMenuActive("ftArea2");
        });
        
        var toolbar = layout.attachToolbar();
        toolbar.setIconsPath("img/");
        toolbar.loadStruct("data/toolbar_area2.xml");
        
        var pagingId = "pagingArea_"+window.dhx.newId();
        var statusbar = layout.attachStatusBar({
            text: "<div id='"+pagingId+"'></div>",
            paging: true
        });
        
        var detectsGrid = layout.cells("a").attachGrid();
        detectsGrid.setHeader("Rendszám,Detekció ideje,Chip,Azonosító,Pozíció,Kuka űrtartalom,Gyűjtési napok,Szerződéskód,Cím");   
        detectsGrid.setColumnIds("plate_number,detect_time,chip,_code,position,space,days,code,address");        
        detectsGrid.setInitWidths("100,170,170,100,*,*,*,*,*");  
        detectsGrid.setColAlign("left,left,left,left,left,left,left,left,left");     
        detectsGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");               
        detectsGrid.setColSorting("str,str,str,str,str,str,str,str,str");  

        detectsGrid.i18n.paging={
              results:"Találatok",
              records:"Listázva ",
              to:" -től ",
              page:"Oldal ",
              perpage:"sor egy oldalon",
              first:"Első oldalhoz",
              previous:"Előző",
              found:"Found records",
              next:"Következő",
              last:"Utolsó oldalhoz",
              of:" -ból ",
              notfound:"Nincs találat" 
        };
        detectsGrid.setImagePath("codebase/imgs/");
        detectsGrid.enablePaging(true, 15, 5, pagingId);
        detectsGrid.setPagingSkin("toolbar");

        detectsGrid.init();
        detectsGrid.load("data/query.php?cmd=detect");  
        detectsGrid.attachHeader("#text_filter,#daterange_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        
        toolbar.attachEvent("onclick",function(id){
          if(id=="export_csv"){       
            detectsGrid.setCSVDelimiter("|");                  
            var myCSVString = detectsGrid.serializeToCSV();
            document.getElementById('export_csv_input').value = myCSVString;
            window.open('', 'CSV');
            document.getElementById('ExportCSVForm').submit();
          };
        });
        

  
      });
    </script>    
 